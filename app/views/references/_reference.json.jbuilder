json.extract! reference, :id, :reference_name, :created_at, :updated_at
json.url reference_url(reference, format: :json)