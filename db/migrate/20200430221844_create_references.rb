class CreateReferences < ActiveRecord::Migration[5.0]
  def change
    create_table :references do |t|
      t.string :reference_name

      t.timestamps
    end
  end
end
